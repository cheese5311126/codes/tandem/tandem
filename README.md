# Tandem

[![SQAaaS badge shields.io](https://img.shields.io/badge/sqaaas%20software-silver-silver)](https://eu.badgr.com/public/assertions/nM7pO3T0Sq2f1klkQ6gAGA "SQAaaS silver badge achieved")
[![GitLab Release (latest by SemVer)](https://img.shields.io/github/v/release/TEAR-ERC/tandem)](https://github.com/TEAR-ERC/tandem/releases)


Tandem is a scalable discontinuous Galerkin code on unstructured curvilinear grids for linear elasticity problems and
sequences of earthquakes and aseismic slip. Tandem uses the symmetric interior penalty discontinuous Galerkin (SIPG)
method to perform seismic cycle simulations accounting for the complex geometries and heterogeneity of the subsurface.
Tandem simulates fault behavior over multiple seismic events and the intervening periods of aseismic deformation and
considers all phases of earthquake faulting, from slow loading to earthquake nucleation, propagation and termination
over time scales of milliseconds to millennia in a unified, self-consistent framework, opening new avenues to pursue
extreme scale 3D earthquake cycle simulations.

## Documentation

For further information, please visit the Tandem 
[User Guide](https://tandem.readthedocs.io/en/latest/).

## Developers

authors:
  - Carsten Uphoff
  - Dave May
  - Alice-Agnes Gabriel
  - Jeena Yun
  - Thomas Ulrich
  - Nico Schliwa
  - Casper Pranger

## License

tandem is made available under the [BSD 3-Clause License](https://github.com/TEAR-ERC/tandem/blob/main/LICENSE.md).

BSD 3-Clause License

Copyright (c) 2020, Ludwig-Maximilians-Universität München All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

    Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


